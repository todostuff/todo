import Canvas from './Componets/Canvas';
import './App.css';

function App() {
  return (
    <div className="App">
        <Canvas />
    </div>
  );
}

export default App;
